package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginAlphaNumRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "taranpreet10" ) );
	}
	@Test
	public void testIsValidLoginAlphaNumBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramse1" ) );
	}
	@Test
	public void testIsValidLoginAlphaNumBoundaryOut( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "@ramse1" ) );
	}
	@Test
	public void testIsValidLoginAlphaNumException( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "1ramse" ) );
	}

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses1" ) );
	}

	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramse1" ) );
	}
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rame1" ) );
	}

	@Test
	public void testIsValidLoginLengthException( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ram1" ) );
	}

}
